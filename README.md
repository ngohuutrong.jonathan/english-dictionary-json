# English Dictionary JSON

This is just a JSON file of English definitions from the [1913 Webster's English Dictionary](https://www.gutenberg.org/ebooks/29765).
The original dictionary has several definitions and related information for many words but this file has taken everything
out of them except for their most simple definition. 

